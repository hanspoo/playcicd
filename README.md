# playcicd

Build a docker image of a play 1.x project.

Main ideas:

- Use our custom (really big base play image)
- Testing different configuration for dev, uat and prod.
- Testing usage of secrets for distinct enviroments.
- Trigger k8s updates in our onprem k8s cluster
